import unittest

from pyGauss import *

class TestIsoFunctions(unittest.TestCase):
	def setUp(self):
		# TODO: generate smaller deployable profile for unit test
		filename = '../profiles/example.binary.bgData'

		self.p = BeamProfile()
		# only look at first frame.
		self.p.loadFromBgData(filename, '1')


	def testCalculateCentroid(self):
		c = self.p.calculateCentroid()
		self.assertAlmostEqual(c[0], 9.80748508e2, places=5)
		# note: conversation to set (0,0) to left to corner
		self.assertAlmostEqual(c[1], self.p.h-1-6.08280629e2, places=5)

	def testCalculateIsoRadius(self):
		# convert to um:
		self.assertAlmostEqual(2*self.p.calculateIsoRadius(), 1.0809323e3 * 4.4, places=4)

	def testCalculateIsoRadii(self):
		rs = self.p.calculateIsoRadii()
		self.assertAlmostEqual(2*rs[0]/4.4, 1.1247877e3, places=4)
		self.assertAlmostEqual(2*rs[1]/4.4, 1.0352207e3, places=4)

	def testCalculateSkewIsoEccentricity(self):
		self.assertAlmostEqual(self.p.calculateSkewIsoEccentricity(), 0.4124817)


if __name__ == '__main__':  # execute only if starting from scratch and not if imported.
	unittest.main()
