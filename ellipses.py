# functions in this file were thankfully taken from
# http://nicky.vanforeest.com/misc/fitEllipse/fitEllipse.html


import pylab as _pl
from numpy import abs as _abs
from numpy.linalg import eig as _eig, inv as _inv

def fitEllipse(x,y):
  """
  fits an ellipses through the x and y values obtained, by using
  0 = f(a,(x,y)) = D * a

  Parameters:
  -----------
  x : array
  	array of x values
  y : array
  	array of y values

  Returns:
  --------
  a : numpy.array
  	vector that contains the coefficients for xx, xy, yy, x, y, 1
  """
  x = _pl.float_(x)
  y = _pl.float_(y)
  x = x[:,_pl.newaxis]
  y = y[:,_pl.newaxis]
  D = _pl.hstack((x*x, x*y, y*y, x, y, _pl.ones_like(x)))
  S = _pl.dot(D.T,D)
  C = _pl.zeros([6,6])
  C[0,2] = C[2,0] = 2; C[1,1] = -1

  E, V =  _eig(_pl.dot(_inv(S), C))
  n = _pl.argmax(_abs(E))
  a = V[:,n]
  return a

def ellipse_axis_length( a ):
  """calculates the axis length for a given ellipse coefficient vector a.
  See fitEllipse for details"""
  b,c,d,f,g,a = a[1]/2, a[2], a[3]/2, a[4]/2, a[5], a[0]
  up = 2*(a*f*f+c*d*d+g*b*b-2*b*d*f-a*c*g)
  down1=(b*b-a*c)*( (c-a)*_pl.sqrt(1+4*b*b/((a-c)*(a-c)))-(c+a))
  down2=(b*b-a*c)*( (a-c)*_pl.sqrt(1+4*b*b/((a-c)*(a-c)))-(c+a))
  res1=_pl.sqrt(abs(up/down1))
  res2=_pl.sqrt(abs(up/down2))
  return _pl.array([res1, res2])

def ellipse_center(a):
  """calculates the ellipse's center for a given ellipse coefficient vector a.
  See fitEllipse for details"""
  b,c,d,f,g,a = a[1]/2, a[2], a[3]/2, a[4]/2, a[5], a[0]
  num = b*b-a*c
  x0=(c*d-b*f)/num
  y0=(a*f-b*d)/num
  return _pl.array([x0,y0])

def ellipse_angle_of_rotation( a ):
  """calculates the angle of rotation in rad for a given ellipse coefficient vector a.
  See fitEllipse for details"""
  b,c,d,f,g,a = a[1]/2, a[2], a[3]/2, a[4]/2, a[5], a[0]
  return 0.5*_pl.arctan(2*b/(a-c))
