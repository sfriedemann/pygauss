Install
=======
Dependencies
------------
- python3.3, best use [WinPython][winpython] on Windows.
- for Zemax-Beamfile reading and writing, [PyZDDE][pyzdde], at least
version 0.8.01 - Sep 02, 2014 is needed to be in python path.
The Zemax version of July 2011 (32Bit) was used for testing.
- BeamGage binary file reading and writing was tested with OPHIR Photonics BeamGage 6.1 Standard.

Setup path
----------
To install this library, create a file called `pyGauss.pth`
in `PYTHONDIR/Lib/site-packages` containing the path of the
`pyGauss`-directory:
```
C:\Users\USERNAME\pyGauss
```

To set `example/BeamViewer.py` as the default Application for opening BeamFiles (.zbf and .bgData):
Windows
-------
Create a file `BeamViewer.cmd` containing something like
```
"C:\Users\%USERNAME%\Downloads\WinPython-64bit-3.3.5.0\scripts\python.bat" "C:\Users\%USERNAME%\pyGauss\examples\BeamViewer.py" %1
```
and select it in the _open with_ - dialog of .zbf and .bgData files.

Linux - Gnome3
--------------
```
touch ~/.local/share/applications/BeamViewer.desktop
```
and insert
```
[Desktop Entry]
Type=Application
Encoding=UTF-8
Name=BeamViewer
Comment=to view Beamfiles in different formats
Exec=/usr/bin/env python3 /path/to/pyGauss/examples/BeamViewer.py %U
Icon=application.png
Terminal=true
```
and select it in the _open with_ - dialog of .zbf and .bgData files in nautilus.




[winpython]: http://winpython.sourceforge.net/
[pyzdde]:    https://github.com/indranilsinharoy/PyZDDE
