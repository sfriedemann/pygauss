# -*- coding: utf-8 -*-


# Note: Units for all intern calculations are px.

# documentation using this style:
# http://sphinxcontrib-napoleon.readthedocs.org/en/latest/example_numpy.html

import scipy.optimize as _optimize
from scipy import exp as _exp, ndimage as _ndimage
import re as _re

import matplotlib.cm as _cm

# for imshow
import pylab as _pl
from numpy import abs as _abs

# for beamgage read
import h5py as _h5py

import ellipses as _el

from math import isnan as _isnan

import sys

# lru_cache does not work in python2. So determine version:
lru_cache = None
if sys.version_info.major >= 3 and sys.version_info.minor >= 2:
	from functools import lru_cache
else:
	# TODO: does not work properly. So no python2.7 support so far.
	lru_cache = lambda *args: args[0](*(args[1:]))


try:
	import pyzdde.zdde as _pyz
except ImportError:
	print('pyzdde was not found in python path. Some functions of pyGauss will be disabled')

_isDebug = True


def dPrint(a):
	if _isDebug:
		print(a)


class FitResult:
	"""Represents a fit result after a gauss fit through the beam profile"""
	# low: maybe let BeamProfile remember last fit to not need to calculate so much

	def __init__(self, values, func, popt=None, pcov=None, error=False):
		"""constructs a FitResult.

		Parameters
		----------
		values
			list of values that were fitted. These will be used for further calculation
		func : callable
			The function which was used to generate this FitResult
		popt :
			The optimised parameters for func
		pcov :
			The Covariance-Matrix, see scipy.optimize.curve_fit
		error : Boolean
			True if the FitResult is probably wrong as errors occurred during fit.
		"""
		self._noError = not error
		if error:
			return
		# contains the fit function
		self.func = func

		self.values = values

		# contains optimal values after intensity fit.
		# contains None for fits that did not work.
		self.popt = popt

		# contains covariance matrix of the fit.
		# contains None for fits that did not work.
		self.pcov = pcov

	def isValid(self):
		"""returns true if the Result was fitted without any errors and therefore is valid."""
		return not self._noError

	def getFuncName(self):
		"""returns the fitting function's name"""
		assert self._noError
		return self.func.__name__

	# self.popts[len(self.popts)/2] <=> index of y-axis, intensity function-popts
	def getW_z(self):
		"""returns fitted w_z in px"""
		assert self._noError
		return abs(self.popt[2])

	def calculateError(self):
		"""returns the standard deviation and the max absolute difference between
		measured values and fitted ones.
		"""
		assert self._noError

		n = len(self.values)
		fittedValues = self.func(_pl.arange(n), *(self.popt))

		# diag = None
		# try:
		# print ("test")
		# diag = _pl.diag(self.pcov)
		# except:
		# diag = self.pcov
		# print(_pl.sqrt(diag))
		diffs = _pl.array(self.values)-fittedValues
		return _pl.sqrt((diffs**2).sum() / n), diffs.max()

	def getRelErrorMinMaxMean(self):
		"""returns the minimal, maximum and mean relative error between measuredValues and fit
		Returns
		-------
			(min, max, mean) : tupel
				error min, error max, error mean
		"""
		fittedValues = self.func(_pl.arange(len(self.values)), *(self.popt))
		diffs = _abs(_pl.array(self.values) - fittedValues) / fittedValues
		return (diffs.min(), diffs.max(), diffs.mean())

	def calculateMaxI(self):
		"""returns the max of the fitted intensity function."""
		return self.popt[0]/(self.popt[2]**2)


# REM: lru_cache is not per instance but per function.
# It caches most 128 recently called functions.
@lru_cache()
def getIntensityFunctions(A_0=None, mean=None, w=None):
	"""Generates an array of intensity functions for fit.
	All combinations of fixed parameters are Returned. If a parameter is fixed,
	the obtained arguments in getIntensityFunctions are used.

	Intensity - function for a gaussian beam:
		A_0/(w**2) * _exp(-2*((x-mean)/w)**2)

	Parameters
	----------
		A_0 : float
			A_0 = I_0 * (w_0/w)**2
			if None this Parameter will always be fitted

		mean : float
			the x-Position of the centroid.
			if None this Parameter will always be fitted

		w : float
			the radius of the beam where It's intensity I = exp(-2) I_0
			if None this Parameter will always be fitted

	Returns
	-------
		Array of intensityFunctions
	"""
	# fit everything
	def intensity(x, A_0, mean, w):
		return A_0/(w**2) * _exp(-2*((x-mean)/w)**2)

	# tricky: using closures in python - seems to work:
	# empty parameters are taken from getIntensityFunctions' arguments
	def intensityFit_A_0(x, A_0, empty1, empty2):
		return intensity(x, A_0, mean, w)

	def intensityFit_Mean(x, empty1, mean, empty2):
		return intensity(x, A_0, mean, w)

	def intensityFit_W(x, A_0, empty1, empty2, w):
		return intensity(x, A_0, mean, w)

	def intensityFit_A_0_Mean(x, A_0, mean, empty1):
		return intensity(x, A_0, mean, w)

	def intensityFit_A_0_W(x, A_0, empty1, w):
		return intensity(x, A_0, mean, w)

	def intensityFit_Mean_W(x, empty1, mean, w):
		return intensity(x, A_0, mean, w)



	functions = [intensity]
	if A_0 is not None:
		functions.append(intensityFit_Mean_W)
		if mean is not None:
			functions.append(intensityFit_W)  # A_0 and mean are given -> fit for W...
		if w is not None:
			functions.append(intensityFit_Mean)

	if mean is not None:
		functions.append(intensityFit_A_0_W)
		if w is not None:
			functions.append(intensityFit_A_0)

	if w is not None:
		functions.append(intensityFit_A_0_Mean)

	return functions


class BeamProfile:
	"""Represents a beam profile in a specific point
	the unit of all lengths is pixel!
	"""


	# Defines the conversation from micrometer to pixel.
	# The standard case uses the "ophir optics - Spiricon SP620U": 1 px <=> 4.4 um
	def __init__(self, P=None, data=None):
		self.P = P
		self.data = None
		self.w = None
		self.h = None
		self.sum = 0 if not data else data.sum()
		self._fileName = ''
		self._frameName = ''
		self._pixUm = [4.4, 4.4]

	def toUm(self, pixel, direction=None):
		"""Converts pixel length to um length.
		Parameters
		----------
		pixel : int
			length in pixel
		direction : int
			0 for x, 1 for y, optional if _pixUm[0]==_pixUm[1]
		"""
		if direction is None:
			if self._pixUm[0] != self._pixUm[1]:
				print("Warning!: pixUmX != pixUmY - better use toUmX and toUmY")
				assert False
			direction = 0
		return pixel * self._pixUm[direction]



	def toPixel(self, um, direction=None):
		"""Converts um length to pixel length.
		Parameters
		----------
		um : float
			length in um
		direction : int
			0 for x, 1 for y, optional if _pixUm[0]==_pixUm[1]
		"""
		if direction is None:
			if self._pixUm[0] != self._pixUm[1]:
				print("Warning!: pixUmX != pixUmY - better use toUmX and toUmY")
				assert False
			direction = 0
		return um / self._pixUm[direction]

	def convertToWperUm(self, x):
		"""converts single value from zbf to W/um, if was read from a zbf-file"""
		return x / (self._pixUm[0]*self._pixUm[1]) if _re.search('.*\.zbf', self._fileName, flags=_re.IGNORECASE) else x

	def toWperUm(self):
		"""converts zbf data to W/um, returns self.data non-zbf"""
		return self.convertToWperUm(self.data)

	def _assertData(self):
		if self.data is None:
			raise ValueError("self.data is empty. E.g. use loadFromBgData or loadFromMeanBgData first")

	def _initConsts(self):
		"""inits self.h, self.w, self.sum . Must be called after self.data was assigned."""
		self.h, self.w = self.data.shape
		self.sum = self.data.sum()
		dPrint(self.sum)

	def getUnitString(self):
		"""returns unit name of self.data as a string."""
		return 'W/(µm²)' if _re.search('.*\.zbf', self._fileName, flags=_re.IGNORECASE) else 'Cnts'

	def loadFromImage(self, filename, channel=0, pixUm=None):
		"""Mainly for testing purpose
		Parameters
		----------
		channel : int
			which channel to user (0:r, 1:g, 2:b)
		pixUm : [float, float] or None
			um per px in X- / Y-Direction
		"""
		self._fileName = filename
		self._frameName = (['r', 'g', 'b'])[channel]
		self._pixUm = pixUm
		pix = _pl.imread(filename)
		self.data = pix.transpose()[channel].transpose()
		self._initConsts()


	def loadFromBgData(self, filename, strFrame):
		"""Reads in a BeamGage file, "normalizes" it (subtracts min) and
		returns it as well as its width and its height
		"""
		self._fileName = filename
		self._frameName = strFrame
		f = _h5py.File(filename, 'r')
		one = f['BG_DATA'][strFrame]

		self.w = one['RAWFRAME']['WIDTH'][0]
		self.h = one['RAWFRAME']['HEIGHT'][0]
		# REM: these fields were not documented in Beamgage-User-Guide.pdf - but they seem to work
		xum = float(one['RAWFRAME']['PIXELSCALEXUM'][0])
		yum = float(one['RAWFRAME']['PIXELSCALEYUM'][0])
		if xum != 0 and yum != 0:
			self._pixUm = [xum, yum]


		# transform to counts
		re = _re.search('([LRS])([\d]*)(_([\d]*))', str(one['RAWFRAME']['BITENCODING'][0]))
		bits = re.group(4) if len(re.groups()) == 4 else re.group(2)

		self.data = _pl.float_(one['DATA'])
		# if r-justified.... (TODO: TEST!)
		emptyBits = int(re.group(2)) if re.group(1) == 'R' else 0
		factor = (_pl.float_(2)**int(bits)-1)/(_pl.float_(2**(31-emptyBits)-1))
		# S16_13

		self.data *= factor
		# -> scaling factor should not matter for centroid and gauss fit after
		# scaling to P.
		# does not deliver the same values as beamgage...
		f.close()

		# bring to array form...
		self.data = _pl.reshape(self.data, (self.h, self.w))

		self.sum = self.data.sum()

		return self.data, self.w, self.h

	def loadFromMeanBgData(self, filename, startFrame=None, frameCount=None):
		"""Reads in a beamgage file and means over Frames

		Parameters
		----------
		startFrame : integer
			the frame where to Start
		frameCount : integer
			the amount of frames to mean if not None
		"""
		# Note: _pixUm is taken from the last frame, as all should have the same content in this field.
		if not startFrame:
			startFrame = 1
		if not frameCount:
			frameCount = startFrame - 2

		frame = startFrame
		data = None
		while frame != frame + frameCount:
			try:
				frameData = BeamProfile()
				frameData.loadFromBgData(filename, str(frame))
				frameData = frameData.data
				if data is None:
					data = _pl.float_(frameData)
				else:
					data += frameData

				frame += 1
			except:
				# seems to run out of frames
				break
		print("found %d frames" % (frame - startFrame))
		if data is None:
			raise ValueError('No data could be loaded! data is empty.')

		# calculate mean
		self.data = data / (frame - startFrame)
		self._initConsts()
		self._frameName = '%d - %d' %(startFrame, startFrame + frameCount)

		return self.data, self.w, self.h

	def saveToBgData():
		# TODO saveToBgData
		# use h5f dummy....
		# generate spiricon format. set all the things like pixumX, pixumY, bit encoding, settings?

		pass

	def loadFromZbf(self, beamfilename):
		self._frameName = ''
		self._fileName = beamfilename


		(version, (nx, ny), ispol, units, (dx, dy), (zposition_x, zposition_y),
		(rayleigh_x, rayleigh_y), (waist_x, waist_y), lamda, index, re, se,
		(x_matrix, y_matrix), (Ex_real, Ex_imag, Ey_real, Ey_imag)) = _pyz.readBeamFile(beamfilename)

		# REM: low: adapt if x_matrix and y_matrix in _pyz.readBeamFile change!

		self._pixUm = [_pyz.zemaxUnitToMeter(units, dx)*1e6, _pyz.zemaxUnitToMeter(units, dy)*1e6]

		# conversations:
		self.data = _pl.float_(Ex_real)**2 + _pl.float_(Ex_imag)**2
		self.data = self.data.transpose()

		#self.data = _pl.flipud(self.data.transpose())

		self._initConsts()

		self.P = self.sum + (_pl.float_(Ey_real)**2 + _pl.float_(Ey_imag)**2).sum()

		return self.data, self.w, self.h

	def saveToZbf(self, beamfilename, zposition, rayleigh,
				 waist, lamda, index=1, version=1, P=None, zbfSize=(512, 512)):
		"""Saves the beam profile as zemax beam file
		Parameters
		----------
		beamfilename : string
			the filename of the beam file to read
		version : integer
			the file format version number
		ispol : boolean
			is the beam polarized?
		units : integer
			the units of the beam, 0 = mm, 1 = cm, 2 = in, 3  = m
		zposition : 2-tuple, (zpositionx, zpositiony)
			the x and y z position of the beam
		rayleigh : 2-tuple, (rayleighx, rayleighy)
			the x and y rayleigh ranges of the beam
		waist : 2-tuple, (waistx, waisty)
			the x and y waists of the beam
		lamda : double
			the wavelength of the beam
		index : double
			the index of refraction in the current medium

		P : float
			Power of the whole profile, will be used instead of self.P
		zbfSize : 2-tuple, (zbfWidth, zbfHeight)
			size of zbf-file in pixel (= number of samples)
			must be a power of 2 between 32 and 8192
			the data will be scaled to fit it.
		"""
		self._assertData()
		if not P:
			P = self.P
			if not P:
				P = 1
		else:
			self.P = P

		dx = float(self.toUm(1.0*self.w / zbfSize[0]) / 1000)
		dy = float(self.toUm(1.0*self.h / zbfSize[1]) / 1000)

		units = 0  # -> mm

		# no polarization as there is no data for it.
		isPol = False
		efield = self.data;
		#efield = _pl.flipud(self.data)
		efield = _ndimage.zoom(efield, (float(zbfSize[1])/self.h, float(zbfSize[0])/self.w))
		efield -= efield.min()
		# normalize:
		sumEfield = efield.sum()
		efield *= P / (sumEfield)
		# transform to electric field
		efield = _pl.sqrt(efield)


		# transform to correct format for pyz.writeBeamFile
		# no imaginary part, as we have no data for it.

		# note: other implementation of this algorithm can be found in BgData2zbf.py of 01.07.14
		_pyz.writeBeamFile(beamfilename, version, zbfSize, isPol, units, (dx, dy), zposition, rayleigh,
				 waist, lamda, index, 0, 0, (efield, _pl.zeros_like(efield), None, None))

		return self.data, self.w, self.h

	@lru_cache()
	def calculateCentroid(self):
		"""Calculates the centroid of a given 2D array

		Returns
		-------
		(x, y) : tupel
			the coordinates of the calculated centroid
		"""
		# returns the same as beamgage does but mind the coordinate system!
		# x coord is correct but not y...
		self._assertData()

		sumX = 0.0
		sumY = 0.0

		for x, col in enumerate(self.data.transpose()):
			sumX += col.sum() * x
		for y, row in enumerate(self.data):
			sumY += row.sum() * y


		if self.sum == 0.0:
			print('error calculating centroid from %s' % (self._fileName))
			return (-1, -1)

		return (sumX / self.sum, sumY / self.sum)
		# IMPORTANT: to put 0,0 to bottom left for comparism with beamgage, one must set y to self.h-1-y:
		#return (sumX / sumAll, self.h - 1 - sumY / sumAll)


	# let's calculate w2:
	# gets the radius where I = I_max * e**-2 fits
	@lru_cache()
	def calculateGaussianRadii(self):  # deprecated
		"""Calculates different types of gaussian radii for further fitting.
		The radii are measured from centroid to the point where the intensity is
		1/exp(2) of the centroid intensity.

		Returns
		-------
		w : 2-tupel (wx, wy)
			wx radius in x direction in px
			wy radius in y direction in px

		See Also
		--------
		calculateCentroid()

		"""
		self._assertData()
		roundedCentroid = [int(round(r)) for r in self.calculateCentroid()]

		# in x, y, circular direction
		w = [0, 0]

		I_max = self.data[roundedCentroid[1]][roundedCentroid[0]]

		for direction, dataset in enumerate([self.data[roundedCentroid[1]], self.data.transpose()[roundedCentroid[0]]]):

			for dPos in [-1, 1]:
				pos = roundedCentroid[direction]

				while 0 < pos < len(dataset) and dataset[pos] > I_max * _exp(-2):
					pos += dPos

				if(pos >= len(dataset)):
					raise ValueError('w out of bounds of image')

				# meaning meaning for dpos = 1 and dpos = -1
				w[direction] += abs(pos - roundedCentroid[direction]) / 2.0


		return w

	def calculateGaussianCircularRadius(self):	# low: unused as it takes too long?
		centroid = self.calculateCentroid()
		P0 = [100000, centroid[0], centroid[1], self.w/4]

		screenRatio = self.w/self.h

		def intensityCircular(r, A, meanX, meanY, w_cir):
			return A / w_cir**2 * _exp(-2*((screenRatio*(_pl.int_(r / self.w) - meanY))**2
				+ (r % self.w - meanX)**2)/(w_cir**2))

		values = self.data.reshape((self.w * self.h))
		v_range = _pl.arange(len(values))

		popt, pcov = _optimize.curve_fit(intensityCircular, v_range, values, p0=P0)
		#_pl.plot(values)
		#_pl.plot(v_range, intensityCircular(v_range, *popt), label=('fit'))
		#_pl.show()
		v_range = _pl.arange(self.w*2)

		w = abs(popt[3])  # as it might be negative after fit.
		return w

	def inProfil(self, x):
		"""checks if x is in range of self.data. x: 2D-vector"""
		return (0 < x[0] < self.w) and (0 < x[1] < self.h)

	def findIsoPoints(self, level, dLevel=0):
		"""searches for points that match an isoline + dLevel

		Parameters
		----------
		level : float
			the level to mark
		dLevel : float
			the accuracy of the level

		Returns
		-------
		xs : int list
			x coordinates of points
		ys : int list
			y coordinates of points

		"""
		xs = []
		ys = []
		a = ((level <= self.data) & (self.data < level+dLevel))
		for y in range(self.h):
			for x in range(self.w):
				if a[y, x]:
					xs.append(x)
					ys.append(y)

		return xs, ys


	def findRadialIsoLine(self, origin, level, steps=400, dr=1):
		"""iterates circular around origin and finds an isoline.

		Parameters
		----------
		origin : 2-tupel(x, y)
			origin of Radial isoline to fit
		level : float
			isoline will be where self.data < level,
			start checking from origin.
		steps : int
			angle steps to do, optional
		dr : float
			radius step size, optional



		Returns
		-------
		xs, ys: 2-tupel of lists
			containing xs and ys values, origin centered.
		"""
		xs = []
		ys = []

		for i in range(steps):
			angle = i * 2 * _pl.pi / steps
			r = 0
			p = int(origin[0]), int(origin[1])
			while self.inProfil(p) and self.data[int(p[1]), int(p[0])] > level:
				p = _pl.array([_pl.cos(angle), _pl.sin(angle)]) * r + origin
				r += dr

			xs.append(p[0]-origin[0])
			ys.append(p[1]-origin[1])

		return xs, ys

	def calculatePerfectLevel(self, steps=400):
		c = self.calculateCentroid()
		r = _pl.array(c).min() / 3.0
		vals = []

		angles = _pl.linspace(0, 2*_pl.pi, num=steps)
		for angle in angles:
			p = _pl.array([_pl.cos(angle), _pl.sin(angle)]) * r + c
			vals.append(self.data[int(p[1]), int(p[0])])

		vals = _pl.array(vals)
		return vals.mean(), vals.std()

	################# ISO functions.
	# http://stackoverflow.com/questions/9005659/compute-eigenvectors-of-image-in-python/9007249#9007249
	# ^gave me soo much performance :)
	@lru_cache()
	def calculateIsoRadius(self):
		"""calculates ISO 11145 radius in um, assuming that all the power is on the screen.
		For reference see BeamGage manual v5.9 8/21/2012
		Is exactly the same as dsigma4 in BeamGage.
		"""
		c = self.calculateCentroid()

		y, x = _pl.float_(_pl.mgrid[:self.h, :self.w])
		x -= c[0]
		y -= c[1]
		data = self.data * (self.toUm(x, 0)**2 + self.toUm(y, 1)**2)
		sigma_squared = data.sum()

		sigma_squared /= self.sum

		d = 2*(2**0.5)*(sigma_squared**0.5)
		w = d/2
		return w

	@lru_cache()
	def _calculateLaboratorySigmasSquared(self):
		"""calculates the second moments sigma^2_xx, sigma^2_yy, sigma^2_xy of the
		laboratory system in um"""
		c = self.calculateCentroid()
		y, x = _pl.float_(_pl.mgrid[:self.h, :self.w])
		x -= c[0]
		y -= c[1]

		# very important to keep ratio:
		x = self.toUm(x, 0)
		y = self.toUm(y, 1)

		dataxx = self.data * x**2
		datayy = self.data * y**2
		dataxy = self.data * x * y

		sxx = dataxx.sum() / self.sum
		syy = datayy.sum() / self.sum
		sxy = dataxy.sum() / self.sum

		return sxx, syy, sxy

	# @lru_cache() if it is used by fitGaussianBeam() for getting first radius!
	def calculateIsoRadii(self):
		"""calculates d_sigmaX, d_sigmaY since ISO 11145 amd ISO 13694 in um.
		Gives the same as d4sigmaX,Y in BeamGage.
		"""
		# squared sigmas:
		sx, sy, sxy = self._calculateLaboratorySigmasSquared()

		dsx = 4 * _pl.sqrt(sx)
		dsy = 4 * _pl.sqrt(sy)

		wx = dsx / 2
		wy = dsy / 2
		return wx, wy

	def calculateIsoEccentricity(self):
		"""calculates ISO Eccentricity assuming that laboratory coordinate system and
		beam system are equal."""
		wx, wy = self.calculateIsoRadii()
		dx, dy = 2 * wx, 2 * wy
		return _pl.sqrt(dx**2 - dy**2) / dx

	def calculateSkewIsoEccentricity(self):
		"""since ISO 13694 3.2.5, uses technique introduced in ISO 11146 4.3, 7.2"""
		# squared sigmas:
		sx, sy, sxy = self._calculateLaboratorySigmasSquared()

		eps = (sx-sy)/abs(sx-sy)  # = sgn(sx-sy)

		dsx = 2**1.5 * ((sx+sy) + eps*((sx-sy)**2+4*sxy**2)**0.5)**0.5
		dsy = 2**1.5 * ((sx+sy) - eps*((sx-sy)**2+4*sxy**2)**0.5)**0.5




		# correct order...:
		if dsx < dsy:
			dsx, dsy = dsy, dsx

		ecc = _pl.sqrt(dsx**2-dsy**2)/dsx
		return ecc


#### tries to fit eccentricity...
	def calculateEccentricity(self, plotProfile=False, force=True, where=0.8, ps=None, plotFit=True, algoNo=0):
		"""determines eccentricity by getting contour lines and then fit ellipsis in
		it to get	its eccentricity

		Parameters
		----------
		plotProfile : bool
			default=False, if True, the BeamProfile and the found ellipses points are plotted.
		force : bool
			default=True, if True, calculateEccentricity2 is used in case of unfitable contour lines.
		where : float
			the fraction from max of where to fit the ellipse
			for useful results where should be 0.5 < where < 0.95
		ps : None or tupel(xs, ys)
			if not none, xs, ys are taken for plot
		plotFit : bool
			default=True, if True and plotProfile is True, the contour line fitted as ellipse is plotted -> good for debugging.
		algoNo : int
			default=0, which algorithm to use for finding ellipsis points. See source for details.

		Returns
		-------
		the calculated numerical eccentricity. This equals 0 for circles.
		"""

		centroid = self.calculateCentroid()
		epsilons = []

		# to convert y-values in x-values:
		yRatio = self._pixUm[1] / self._pixUm[0]


		xs = []
		ys = []
		if (ps == None):
			mean = self.data.mean()
			I_max = self.calculateMaxI()
			level = I_max * where
			if algoNo == 0:
				xs, ys = self.findRadialIsoLine(centroid, level)
			else:
				xs, ys = self.findIsoPoints(level, I_max*0.05)
				xs -= centroid[0]
				ys -= centroid[1]
			#self.data = _pl.log(self.data)

		else:
			xs, ys = ps
			print(xs)
			print(ys)

		try:
			ell = _el.fitEllipse(xs, ys)
			center = _el.ellipse_center(ell)
			axes = _el.ellipse_axis_length(ell)
			a = axes.max()
			b = axes.min()
			eps = _pl.sqrt(a**2 - b**2)/a
		except:
			eps = _pl.nan

		# calculate eccentricity
		if _isnan(eps) and force:
			print('first try of calculating eps gave nan. Using alternative algorithm...')
			eps = self.calculateEccentricity2(plotProfile)
			plotProfile = False


		centroid = [self.toUm(x, i) for i, x in enumerate(centroid)]
		xs = [self.toUm(x, 0) for x in xs]
		ys = [self.toUm(-x, 1) for x in ys]

		if plotProfile:
			if not _isnan(eps):
				print('a=%f, b=%f, -> eps=%f' % (a, b, eps))


			w = self.toUm(self.w, 0)
			h = self.toUm(self.h, 1)
			extent = (-centroid[0], w - centroid[0], centroid[1]-h, centroid[1])
			im = _pl.imshow(self.toWperUm(),
					extent=extent)
			#_pl.scatter(xs, ys, s=2)
			# plot seems faster...
			_pl.plot(xs, ys, linestyle='None', marker='.', color='black')
			_pl.axis(list(extent))
			ax = _pl.gca()
			ax.set_autoscale_on(False)
			_pl.xlabel('x [µm]')
			_pl.ylabel('y [µm]')
			if plotFit and not _isnan(eps):
				phi = _el.ellipse_angle_of_rotation(ell)
				R = _pl.arange(0, 2*_pl.pi, 0.05)
				xx = self.toUm(center[0] + a*_pl.cos(R)*_pl.cos(phi) - b*_pl.sin(R)*_pl.sin(phi), 0)
				yy = self.toUm(center[1] + a*_pl.cos(R)*_pl.sin(phi) + b*_pl.sin(R)*_pl.cos(phi), 1)

				_pl.plot(xx, yy, color='red')

			cb = _pl.colorbar(im)
			unit = self.getUnitString()
			cb.set_label('I [%s]' % unit)
			_pl.show()

		return eps

	def calculateEccentricity2(self, plotFit=False):
		"""determines eccentricity by getting contour lines and then fit ellipsis in
		it to get	its eccentricity

		Warning: As using contour lines to fit ellipses, interferences will generate wrong
		data.

		Parameters
		----------
		plotFit : Boolean
			if true, the contour lines fitted as ellipses are plotted, makes sense for
			Debug reasons.

		Returns
		-------
		the calculated numerical eccentricity. This equals 0 for circles.
		"""

		centroid = self.calculateCentroid()
		# TODO: calculate too for hyperbels
		epsilons = []

		# to convert y-values in x-values:
		yRatio = self._pixUm[1] / self._pixUm[0]

		# helpful: http://stackoverflow.com/questions/5666056/matplotlib-extracting-data-from-contour-lines/5666461#5666461
		# do a standard contour plot. extract vertices.
		# plot invisible - that approach is kinda dirty but contours design always
		# needs to plot...
		f, ax = _pl.subplots(1, 1)
		cs = ax.contour(self.data)
		f.clear()
		ax.clear()
		del f
		del ax

		colors = iter(_cm.rainbow(_pl.linspace(0, 1, 1000*len(cs.collections))))

		for contour in cs.collections:
			xs = []
			ys = []
			color = next(colors)
			for p in contour.get_paths():
				for v in p.vertices:
					xs.append(v[0] - centroid[0])
					ys.append((v[1] - centroid[1]) * yRatio)

			# now fit in ellipse with the power of this page:
			# http://nicky.vanforeest.com/misc/fitEllipse/fitEllipse.html
			ell = _el.fitEllipse(xs, ys)
			center = _el.ellipse_center(ell)
			axes = _el.ellipse_axis_length(ell)

			a = axes.max()
			b = axes.min()
			eps = _pl.sqrt(a**2 - b**2)/a

			if not _isnan(eps):
				if plotFit:
					print('a=%f, b=%f, -> eps=%f' % (a, b, eps))

					phi = _el.ellipse_angle_of_rotation(ell)
					R = _pl.arange(0, 2*_pl.pi, 0.05)
					xx = center[0] + a*_pl.cos(R)*_pl.cos(phi) - b*_pl.sin(R)*_pl.sin(phi)
					yy = center[1] + a*_pl.cos(R)*_pl.sin(phi) + b*_pl.sin(R)*_pl.cos(phi)

					_pl.plot(xx, yy, color=color)

					_pl.scatter(xs, ys, s=1, color=color)

				epsilons.append(eps)

		if plotFit:
			_pl.show()


		return _pl.float_(epsilons).mean()

	@lru_cache()
	def calculateMaxI(self):
		"""Calculates max Intensity by using max of fits. Will be used to get iso value...
		Note
		----
		means x- and y-value.
		"""

		# maybe take mean from x- and y- centroid value = A_0/w**2

		a = self.fitGaussianBeam(False, fitFunctions=[getIntensityFunctions()[0]])

		Is = []
		for fr in a:
			if fr.getFuncName() == 'intensity':
				Is.append(fr.calculateMaxI())
		return _pl.array(Is).mean()

	def fitGaussianBeam(self, printResults, showProfile=False, showPlot=False, logTitle=None, fitFunctions=None):
		"""Analyzes self.data, tries to fit it with a gaussian beam's intensity profile

		Parameters
		----------
		printResults : boolean
			if True, gauss-fitting results will be printed to stdout

		showProfile : boolean
			if True, plot the beam-profile

		showPlot : boolean
			if not is False, plot the fitted gaussians
			if is a boolean-tupel with 10 elements, only the plots with showPlot[i] == True are done.

		logTitle : string
			if not None this title will be shown when PrintResults.

		Returns
		-------
			fitResults : FitResult
				container with all the gauss fit parameters.
		"""
		self._assertData()

		errorCount = 0

		data = self.data

		width = len(data[0])
		height = len(data)

		if printResults:
			print('*************** File: ' + (logTitle or (self._fileName + '_' + self._frameName)) + ' ********************')
			print("data range: %f, %f, mean: %f" %(data.min(), data.max(), data.mean()))

		if showProfile:
			_pl.imshow(data)
			_pl.show()

		centroid = self.calculateCentroid()
		rcentroid = [int(round(c)) for c in centroid]
		calculatedRadii = None
		try:
			# TODO: for performance: maybe use Iso-Radius here?
			# REM: if analysing the beam profile from -inf to inf: isoRadius == e^-2-Radius...
			calculatedRadii = self.calculateGaussianRadii()
		except ValueError as e:
			print(e)
			# no chance to get better values...
			w = (width + height) / 4.0
			calculatedRadii = [w, w]
			errorCount += 1

		fitResults = []

		for direction, values in enumerate([data[rcentroid[1]], data.transpose()[rcentroid[0]]]):
			# generate fit starting parameters...
			A_0 = 42
			mean = centroid[direction]
			w = calculatedRadii[direction]
			P0 = [A_0, mean, w]

			# function list to iterate for fit, obtain variables that are given
			functions = None
			if fitFunctions == None:
				functions = getIntensityFunctions(None, mean, None if calculatedRadii is None else w)
			else:
				functions = fitFunctions

			v_range = _pl.arange(len(values))
			if showPlot != False and (showPlot is True or (showPlot[direction * 5] == True)):
				_pl.plot(v_range, values[v_range],':', label=('along x' + str(direction) + 'axis'))

			# fit all functions...
			for i, f in enumerate(functions):
				try:
					popt, pcov = _optimize.curve_fit(f, v_range, values, p0=P0, maxfev=10000)
				except RuntimeError:
					fitResults.append(FitResult(values, f, error=True))
				finally:

					if showPlot != False and (showPlot is True or (showPlot[direction*5 + 1 + i] == True)):
						_pl.plot(v_range, f(v_range, *popt), 'o:', label=('fit'+str(i)))

					fitResult = FitResult(values, f, popt, pcov)
					fitResults.append(fitResult)

					if printResults:
						print(popt)
						popt[2] = abs(popt[2])
						print("%s: I_0*w_0^2: %f cnts*px*px, w(z): %f ~ %f um" %
								(fitResult.getFuncName(), popt[0], popt[2], self.toUm(popt[2])))
						err = fitResult.calculateError()
						print("standard deviation: %f -> %.2f %%" % (err, err/(fitResult.calculateMaxI())))
						print("rel error min: %f, rel error max: %f, rel error mean: %f" % fitResult.getRelErrorMinMaxMean())



		if showPlot:
			_pl.legend()
			_pl.title('Fits File ' + self._fileName)
			_pl.xlabel('x (px)')
			_pl.ylabel('y (raw cnts)')
			_pl.show()

		return fitResults


	def calculateFitErrors(self):
		"""calculates fitError and ISO 13694 Roughness of fits through CCD-system
		axis. Means over x- and y-axis"""

		# generate fits...
		frs = self.fitGaussianBeam(False, fitFunctions=[getIntensityFunctions()[0]])

		errs = _pl.array([fr.calculateError() for fr in frs])
		squaredErrors, Rs = errs.T

		max_Is = _pl.array([fr.calculateMaxI() for fr in frs])

		fitError = (squaredErrors/max_Is).mean()
		R = (Rs/max_Is).mean()	# roughness of a fit.

		return fitError, R


	def calculateFixedFitError(self, debugPlot=False):
		"""fits error with fixed: centroid, w0x, w0y"""
		c = self.calculateCentroid()
		rc = [int(cc) for cc in c]	# centroid as integer
		ws = [self.toPixel(x, i) for i, x in enumerate(self.calculateIsoRadii())]
		# generate fit functions
		errs = []
		for direction, values in enumerate([self.data[rc[1]], self.data.transpose()[rc[0]]]):
			P0 = [42, c[direction], ws[direction]]
			f = getIntensityFunctions(mean=P0[1], w=P0[2])[2]
			# REM: even if fitting intensity function 1 would give much nicer results, ISO 11146 defines to only fit for A_0
			# --> intensity function 2
			v_range = _pl.arange(len(values))
			try:
				popt, pcov = _optimize.curve_fit(f, v_range, values, p0=P0, maxfev=10000)
				if debugPlot:
					_pl.plot(v_range, self.convertToWperUm(values))
					_pl.plot(v_range, self.convertToWperUm(f(v_range, *popt)))

					# generate tics:
					numTicks = 10
					l = [self.w, self.h][direction]
					umTick = 10**round(_pl.log(self.toUm(l, direction))/_pl.log(10)-1)


					tick_locs = [c[direction]+i*self.toPixel(umTick, direction) for i in range(round(-numTicks/2), round(numTicks/2))]

					# filter good ones...
					tick_locs = list(filter(lambda x: x >= 0 and x <= l, tick_locs))

					tick_lbls = [self.toUm(t-c[direction], direction) for t in tick_locs]

					# format
					tick_lbls = ['%d' % (round(t)) for t in tick_lbls]

					_pl.xticks(tick_locs, tick_lbls)

					_pl.xlabel(('x' if direction == 0 else 'y') + ' [µm]')
					_pl.ylabel('I [(W/µm²)]')
					_pl.show()
			except RuntimeError:
				dPrint('fitting for fixed Error timed out.')
			finally:
				fr = FitResult(values, f, popt, pcov)
				errs.append(fr.calculateError()[0] / fr.calculateMaxI())
		return _pl.array(errs).mean()

	def calculateFitError(self):
		""" calculates fit error that is used to determine how good a profile matches
		a gaussian beam."""
		return self.calculateFitErrors()[0]

# **********************
#	HELPER FUNCTIONS
# **********************


def M2(w_0, divergence=0.000973, l=1.064):
	"""Calculates the beam quality M2.
	Standard values fit for the given picoblade.

	Parameters
	----------
	w_0 : float
		in um
	divergence : float
		the divergence of the gaussian beam in rad
	l : float
		the wave length of the gaussian beam in um
	"""
	# rad
	pass
	return 2.0*w_0*_pl.pi*divergence / (4.0*l)


def calculateDivergenceError(dx, dD, errorX, errorD):
	"""Calculates the absolute error of the Divergence. See
	Fehlerrechnung-Divergenz.nb
	Important: all parameters must be given in the same unit.
	Important: assuming tan(theta) = theta as the divergence should be very small.

	Parameters
	----------
	dx : float
		difference of diameters, x1 - x2
	dD : float
		difference of distances, d1 - d2
	errorX : float
		error of dx
	errorD : float
		error of dD

	Returns
	-------
	dTheta : float
		the error of the divergence
	"""
	return abs(1.0/dD*errorX) + abs(1.0*dx/(dD**2)*errorD)


def profileFromFile(fname):
	"""Tries to generate a BeamProfile from the specified file.

	Parameters
	----------
	fname : str
		the full path of the file to load
	Returns
	-------
	BeamFile
		the beam profile found in the specified file
	"""
	p = BeamProfile()
	if _re.search('.*\.binary\.bgdata$', fname, flags=_re.IGNORECASE):
		p.loadFromMeanBgData(fname)
	elif _re.search('.*\.zbf$', fname, flags=_re.IGNORECASE):
		p.loadFromZbf(fname)
		print('P=%f W' % (p.P))
	else:
		p.loadFromImage(fname)

	return p


# TODO: unittests. - unit Test for CalculateDivergenceError can be taken from Fehlerrechnung-Divergenz.nb
# doc string unit test since http://openbook.galileocomputing.de/python/python_kapitel_21_005.htm
#if __name__ == "__main__":
#	doctest.testmod()
