Readme
======

this library provides functionality for

* showing,

* converting,

* and analyzing (centroid, radius, eccentricity, fit gaussian ...)

beam files of different formats (.zbf, .binary.bgData ...).

see [INSTALL][INSTALL] for installation instructions











[INSTALL]:https://bitbucket.org/sfriedemann/pygauss/src/HEAD/INSTALL.md
