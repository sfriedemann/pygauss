#!/bin/sh env python3

# this script analysis an obtained file and prints the result to the console.

import sys
from pyGauss import *

from time import time

import pylab as pl

def analyze(filename, showProfile=False):
	"""does some analysis of the obtained BeamProfile and prints the result to the console."""
	time_start = time()

	p = profileFromFile(filename)

	if p.data.sum() == 0:
		print(filename + ' was empty. Returning None')
		return
		#raise ValueError(filename + ' was empty.')

	fitError, R = p.calculateFitErrors()
	fixedFitError = p.calculateFixedFitError()

	# goodness of a fit since Kolomogorov-Smirnov
	# would need erf...
	#delta =
	#G = 1 / (1 + delta * pl.sqrt(p.w * p.h))
	
	#dPrint('standard deviation: %f -> %.2f of max(%.1f)' %
	#	(errs.mean(), (errs/max_Is).mean(), max_Is.mean()))


	#
	#
	#
	#
	dPrint('fixed fit error: %f' % fixedFitError)

	radius = p.calculateIsoRadius()
	dPrint('radius: %f' % radius)

	eccentricity = p.calculateSkewIsoEccentricity()
	dPrint('eccentricity: %f' % eccentricity)

	dPrint('----------------------------------------------')
	dPrint('Analysis took %f seconds' % (time() - time_start))

	if showProfile:
		pl.imshow(p.data)
		pl.show()


	return (p.P or p.data.sum()), fitError, R, fixedFitError, radius, eccentricity

if __name__ == '__main__':
	analyze(sys.argv[1], len(sys.argv) > 2)
	input("Press Enter to continue...")
