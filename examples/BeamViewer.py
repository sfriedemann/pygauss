#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import pylab as pl
import re
from matplotlib.patches import Ellipse
from pyGauss import *

im = None  # last drawn image.
def view(fname, pixel=False, centered=True, markRadius=False, noTitle=False, loga=False, logScale=False, title=None):
	"""if centered and markRadius = True and pixel != True, iso-radius will be calculated and marked.
	if fname is not a String but a BeamProfile, its content will be drawn.
	"""

	global im

	if isinstance(fname, str):
		print('open %s' % fname)
		p = profileFromFile(fname)
	else:
		p = fname

	# low: maybe print frame name?
	print('size[px]: %d x %d' % (p.w, p.h))
	print('size[um]: %f x %f' % (p.toUm(p.w, 0), p.toUm(p.h, 1)))

	if loga:
		# log-feature
		p.data = pl.log(p.data)

	extent = None
	if pixel:
		if centered:
			c = p.calculateCentroid()
			extent = (-c[0], p.w - c[0], c[1]-p.h, c[1])

		im = pl.imshow(p.data, extent=extent)

		pl.xlabel('x [pixel]')
		pl.ylabel('y [pixel]')
	else:
		if centered:
			c = [p.toUm(cc, i) for i, cc in enumerate(p.calculateCentroid())]
			w = p.toUm(p.w, 0)
			h = p.toUm(p.h, 1)
			extent = (-c[0], w - c[0], c[1]-h, c[1])
			if markRadius:
				r = p.calculateIsoRadius()
				print('Radius: %f' % r)
				ell = Ellipse(xy=(0, 0), width=2*r, height=2*r, angle=0, fill=False)
				ax = pl.gca()
				ax.add_patch(ell)

		else:
			extent=(0, p.toUm(p.w, 0), p.toUm(p.h, 1), 0)

		im = pl.imshow(p.toWperUm(), extent=extent)

		pl.xlabel('x [µm]')
		pl.ylabel('y [µm]')

	if not noTitle:
		pl.suptitle(p._fileName)

	cb = pl.colorbar(im)
	if not pixel:
		cb.set_label('I [%s]' % ('log('+p.getUnitString()+')' if logScale else p.getUnitString()))

	if title:
		pl.title(title)



def animate(fnames, pixel=False, centered=True, markRadius=False, noTitle=False, loga=False, loop=False, save=None, title=None):
	print('load all files...')
	ps = []
	for f in fnames:
		ps.append(profileFromFile(f))

	if loga:
		print('log-ing')
		for p in ps:
			p.data = pl.log(p.data)

	counts = -1 if loop else 1
	while counts:
		for i, p in enumerate(ps):
			view(p, pixel, centered, markRadius, noTitle, False, loga, title=title)
			if save:
				pl.savefig(save+str(i)+'.png')
			else:
				pl.draw()
				pl.pause(.1)
			pl.clf()  # clear figure
		counts -= 1


if __name__ == '__main__':
	# in case I'm not included by anybody...
	try:
		# parse input
		title = None
		pixel = False
		radius = False
		loga = False
		centered = True


		if '-t' in sys.argv:
			title = sys.argv[sys.argv.index('-t')+1]
			sys.argv.remove('-t')
			sys.argv.remove(title)

		if '-nc' in sys.argv:
			sys.argv.remove('-nc')
			centered = False

		if '-p' in sys.argv:
			sys.argv.remove('-p')
			pixel = True
		elif '-r' in sys.argv:
			sys.argv.remove('-r')
			radius = True

		noTitle = False
		if '-nt' in sys.argv:
			sys.argv.remove('-nt')
			noTitle = True

		if '-log' in sys.argv:
			sys.argv.remove('-log')
			loga = True

		save = None
		if '-s' in sys.argv:
			save = sys.argv[sys.argv.index('-s')+1]
			sys.argv.remove('-s')
			sys.argv.remove(save)

		if 'animate' in sys.argv:
			print('animating...')
			loop = '-loop' in sys.argv
			if loop:
				print('looping...')
				sys.argv.remove('-loop')
				if save:
				# never loop and save.
					loop = False
			sys.argv.remove('animate')
			fnames = sys.argv[1:]
			animate(fnames, pixel, markRadius=radius, noTitle=noTitle, loga=loga, loop=loop, save=save, centered=centered, title=title)
		else:
			view(sys.argv[1], pixel, markRadius=radius, noTitle=noTitle, loga=loga, logScale=loga, centered=centered, title=title)
			if save:
				pl.savefig(save + '.png')
			else:
				pl.show()
	except:
		print("usage: python BeamViewer.py [(INPUTFILE) | (animate INPUTFILE1 INPUTFILE2 ...) [-p|-r] [-nt] [-log] [-loop] [-nc] [-t TITLE]]"
			"\n -p: x- and y- axis are in pixels. if not specified, x- and y-axis are "
			"in um."
			"\n if not -p: -r can be set to mark the iso radius"
			"\n if -nt no title is rendered"
			"\n if -log will draw logarithmical"
			"\n if animate and -loop will loop endless"
			"\n if -s saves profile/animation to PREFIX.png (does not work with -loop)"
			"\n if -nc don't center"
			"\n if -t TITLE profiles will get an additional title"
			"\n INPUTFILE: a .zbf, .binary.bgData, or image file that will be shown."
			"\n    Additional Information will be obtained via command line."
			"\n    Note: different file types will be recognized by file ending."

		)
		raise  # show error message the hard way.
