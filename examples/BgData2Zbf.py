# -*- coding: utf-8 -*-
import math

from pyGauss import *

import sys
import numpy as np


if len(sys.argv) != 12:
	# print help text
	print("usage: python BgData2Zbf.py INPUTFILE FRAME OUTPUTFILE POWER LAMB WX RX ZX WY RY ZY"
		"\n INPUTFILE:  the path to the input BeamGage file like 'file.binary.bgData'"
		"\n FRAME:      the frame number to use out of the BeamGage-File. If -1 use the mean of all Frames."
		"\n OUTFILE:    the path to the output ZemaxBeamfile (.zbf)"
		"\n POWER:      the power in Watts of the whole beam"
		"\n LAMB:       the wavelength in mm"
		"\n WX, WY:     waist radii in x-, y-direction in mm"
		"\n RX, RY:     rayleigh length of waist in x-, y-direction in mm"
		"\n ZX, ZY:     z-position of waist in x-, y-direction in mm"
		"\n"
		"\n to modify other variables feel free to view the source of this script(BgData2.Zbf)"
	)
else:

	# convert data types...
	for i in range(4, len(sys.argv)):
		sys.argv[i] = np.float_(sys.argv[i])

	scriptname, inputfile, frame, outputfile, power, lamb, wx, Rx, zx, wy, Ry, zy = sys.argv


	prof = BeamProfile()
	if frame == '-1':
		prof.loadFromMeanBgData(inputfile)
	else:
		prof.loadFromBgData(inputfile, frame)
	prof.saveToZbf(outputfile, (zx, zy), (Rx, Ry), (wx, wy), lamb, index=1, version=1, P=power, zbfSize=(512, 512))
